export class Book {
  description: string[];
  isOut: boolean;

  constructor(public title: string) {
    this.isOut = false;
  }
}
