export class Cd {
  description: string[];
  isOut: boolean;

  constructor(public title: string){
    this.isOut=false;
  }
}
