import { Component } from '@angular/core';
import {MenuController, ModalController} from "ionic-angular";
import {Book} from "../../models/Book";
import {MediaServices} from "../../services/media.services";
import {LendBookPage} from "../lend-book/lend-book";


@Component({
  selector: 'page-book-list',
  templateUrl: 'book-list.html',
})

export class BookListPage {

  bookList: Book[];

  constructor(private modalCtrl: ModalController,
              private menuCtrl: MenuController,
              private mediaServices: MediaServices){

  }

  ionViewWillEnter(){
    this.bookList = this.mediaServices.booksList.slice();
  }

  onLoadBook(index:number){
    let modal = this.modalCtrl.create(LendBookPage, {index:index});
    modal.present();
  }

  onToggleMenu(){
    this.menuCtrl.open();
  }
}
