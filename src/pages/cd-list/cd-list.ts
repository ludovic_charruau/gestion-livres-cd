import { Component } from '@angular/core';
import {MenuController, ModalController} from 'ionic-angular';
import {Cd} from "../../models/Cd";
import {MediaServices} from "../../services/media.services";
import {LendCdPage} from "../lend-cd/lend-cd";


@Component({
  selector: 'page-cd-list',
  templateUrl: 'cd-list.html',
})
export class CdListPage {

  cdList: Cd[];

  constructor(private modalCtrl: ModalController,
              private mediaServices: MediaServices,
              private menuCtrl: MenuController) {
  }

  ionViewWillEnter() {
    this.cdList = this.mediaServices.cdList.slice();
  }

  onLoadCd(index: number){
    let modal = this.modalCtrl.create(LendCdPage, {index: index});
    modal.present();
  }

  onToggleMenu(){
    this.menuCtrl.open();
  }


}
