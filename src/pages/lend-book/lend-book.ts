import {Component, OnInit} from '@angular/core';
import {NavParams, ViewController} from 'ionic-angular';
import {Book} from "../../models/Book";
import {MediaServices} from "../../services/media.services";

@Component({
  selector: 'page-lend-book',
  templateUrl: 'lend-book.html',
})
export class LendBookPage implements OnInit{

  index:number;
  book:Book;

  constructor(public navParams: NavParams,
              private viewCtrl: ViewController,
              private mediaServices: MediaServices) {
  }
  ngOnInit(): void {
    this.index = this.navParams.get('index');
    this.book = this.mediaServices.booksList[this.index];
  }

  dismissModal(){
    this.viewCtrl.dismiss();
  }

  onLendMedium (){
    this.mediaServices.onLendMedium(this.index,'book');
  }



}
