import {Component, OnInit} from '@angular/core';
import {NavParams, ViewController} from 'ionic-angular';
import {MediaServices} from "../../services/media.services";
import {Cd} from "../../models/Cd";

@Component({
  selector: 'page-lend-cd',
  templateUrl: 'lend-cd.html',
})
export class LendCdPage implements OnInit {

  index: number;
  cd: Cd;

  constructor(public navParams: NavParams,
              private viewCtrl: ViewController,
              private mediaServices: MediaServices) {
  }

  ngOnInit(): void {
    this.index = this.navParams.get('index');
    this.cd = this.mediaServices.cdList[this.index];
  }

  dismissModal() {
    this.viewCtrl.dismiss();
  }

  onLendMedium(){
    this.mediaServices.onLendMedium(this.index,'cd');
  }

}
