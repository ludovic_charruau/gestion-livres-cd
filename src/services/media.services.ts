import {Book} from "../models/Book";
import {Cd} from "../models/Cd";

export class MediaServices {
  booksList:Book[] = [
    {
      title:'Le Seigneur des procheries',
      description:[
        'Auteur: Tristan Egolf',
        "Résumé: Ce premier roman singulier commence avec la mort,d'un mammouth à l'ère glaciaire et finit par une, burlesque chasse au porc lors d'un enterrement dans, le Midwest d'aujourd'hui. Entre-temps, on aura assisté, à deux inondations, à quatorze bagarres, à trois, incendies criminels...",
        'Éditeur: Gallimard'
      ],
      isOut:false
    },
    {
      title:'Gomorra',
      description: [
        'Auteur: Roberto Saviano',
        'Résumé: Naples et la Campanie sont dominées par la criminalité organisée, - la camorra - sur fond de guerre entre clans rivaux et de trafics en tout genre, contrefaçon, armes, drogues et déchets toxiques.',
        'Éditeur: Gallimard'
      ],
      isOut:false
    }
  ];

  cdList:Cd[]=[
    {
      title:"Lift Your Skinny Fists Like Antennas to Heaven",
      description: [
        'Auteur: Godspeed You! Black Emperor',
        'Label: Kranky',
        'Année: 2000',
        'Genre: post-rock'
      ],
      isOut:false
    },
    {
      title:"Ill Communication",
      description: [
        'Auteur: Bestie Boys',
        'Label: Capitol',
        'Année: 1994',
        'Genre: Break-beat, hip-hop, groove',
      ],
      isOut:false
    },
    {
      title:"Sgt. Pepper's Lonely Hearts Club Band",
      description: [
        'Auteur: Beatles',
        'Label: Parlophone/Capitol',
        'Année: 1967',
        'Genre: Rock',
      ],
      isOut:false
    },
  ];


  onLendMedium(index:number,liste:string){
    if (liste == 'book') {
      this.booksList[index].isOut=!this.booksList[index].isOut;
    }else if (liste == 'cd') {
      this.cdList[index].isOut=!this.cdList[index].isOut;
    }
  }

}
